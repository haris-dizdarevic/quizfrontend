(function(){

    var intercepter = function($q, $rootScope){
        return {
            request: function(config){
                console.log(config);
                config.headers = {
                    'Authorization' : 'Token token=' + window.localStorage.getItem('token'),
                    'Content-Type' : 'application/json'
                };
                return config;
            },
            response: function(result){
                return result;
            },

            responseError: function(rejection) {
                if(rejection.status === 401){
                    window.localStorage.clear();
                    $rootScope.logged = false;
                    $rootScope.$broadcast('unauthorized');
                }
                console.log('Failed with',rejection.status, 'status');
                return $q.reject(rejection);
            }
        }
    };


    quizApp.service('intercepter',intercepter);
}());