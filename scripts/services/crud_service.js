(function(){

    var crud_service = function($http){

        var host = 'https://kviz-service.herokuapp.com/api/v1/';

        this.get_me = function(record){
            var request = $http({
                method: 'get',
                url: host+record
            });
            return request;
        };

        this.post_me = function(source, data) {
            var request = $http({
                method: 'post',
                url: host+source,
                data: data
            });
            return request;
        };

        this.delete_me = function(source){
            var request = $http({
                method: 'delete',
                url: host+source
            });
            return request;
        };

        this.put_me = function(source,data){
            var request = $http({
                method: 'put',
                url: host+source,
                data: data
            });
            return request;
        };

    };

    quizApp.service('crud_service',crud_service);
}());