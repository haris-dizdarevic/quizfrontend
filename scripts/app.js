var quizApp = angular.module('quizApp',['ui.router','ui.bootstrap','angular-flash.service', 'angular-flash.flash-alert-directive','angularUtils.directives.dirPagination']);

quizApp.config(function($stateProvider, $urlRouterProvider, $httpProvider, flashProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'partials/login.html',
            controller: 'authCtrl'
        }).state('userHome', {
            url: '/user_home',
            templateUrl: 'partials/user_home.html',
            controller: 'userHomeCtrl'
        }).state('userHome.listOfQuizzes', {
            url: '/usersQuizzes',
            templateUrl: 'partials/listOfUsersQuizzes.html',
            controller: 'listOfUsersQuizzesCtrl',
            resolve: {
                quizzes: function (crud_service) {
                    return crud_service.get_me('assigned_quizzes');
                }
            }
        }).state('userHome.profile', {
            url: '/profile',
            templateUrl: 'partials/usersProfile.html',
            controller: 'usersProfileCtrl',
            resolve: {
                user: function (crud_service) {
                    return crud_service.get_me('users/'+window.localStorage.getItem('userId'));
                }
            }
        }).state('userHome.quiz', {
            url: '/quiz/:id',
            templateUrl: 'partials/quiz.html',
            controller: 'quizCtrl',
            resolve: {
                quiz: function(crud_service, $stateParams) {
                    return crud_service.get_me('quizzes/'+$stateParams.id);
                }
            }
        }).state('userHome.inputQuiz',{
            url: '/input/quiz/:id',
            templateUrl: 'partials/usersInputQuiz.html',
            controller: 'usersInputQuizCtrl',
            resolve : {
                quiz: function(crud_service, $stateParams){
                    return crud_service.get_me('quizzes/'+$stateParams.id);
                }
            }
        }).state('userHome.results',{
            url: '/results',
            templateUrl: 'partials/usersResults.html',
            controller: 'usersResultsCtrl',
            resolve : {
                results: function (crud_service) {
                    return crud_service.get_me('results');
                }
            }
        })
        .state('adminHome',{
            url: '/admin_home',
            templateUrl: 'partials/admin_home.html',
            controller: 'adminHomeCtrl'
        }).state('adminHome.quizzes',{
            url: '/newQuiz',
            templateUrl: 'partials/createQuiz.html',
            controller: 'newQuizCtrl'
        }).state('adminHome.usersList',{
                url: '/users_list',
                templateUrl: 'partials/usersLists.html',
                controller: 'usersListCtrl',
                resolve:{
                    users: function (crud_service) {
                        return crud_service.get_me('users');
                    }
                }
            }).state('adminHome.quizzesList',{
            url: '/quizzes',
            templateUrl: 'partials/adminHomeQuizTable.html',
            controller: 'quizzesListCtrl',
            resolve:{
                quizzes: function(crud_service){
                    return crud_service.get_me('quizzes');
                }
            }
        }).state('adminHome.savedQuizzes', {
            url: '/savedQuizzes',
            templateUrl: 'partials/savedQuizzes.html',
            controller: 'savedQuizzesCtrl',
            resolve:{
                savedQuizzes : function (crud_service) {
                    return crud_service.get_me('results');
                }
            }
        }).state('adminHome.savedQuiz', {
            url: '/savedQuiz/:id',
            templateUrl: 'partials/savedQuiz.html',
            controller: 'savedQuizCtrl',
            resolve: {
                quiz: function (crud_service, $stateParams) {
                    return crud_service.get_me('results/'+$stateParams.id);
                },
                selectedAnswers: function (crud_service, $stateParams) {
                    return crud_service.get_me('saved_quizzes?result_id='+$stateParams.id);
                },
                answers: function (crud_service, $stateParams) {
                    return crud_service.get_me('saved_answers?result_id='+$stateParams.id);
                }
            }
        }).state('adminHome.inputQuiz',{
            url: '/new/quiz',
            templateUrl: 'partials/inputQuiz.html',
            controller: 'inputQuizCtrl'
        }).state('adminHome.categories',{
            url: '/categories',
            templateUrl: 'partials/categories.html',
            controller: 'categoriesCtrl',
            resolve: {
                categories: function (crud_service) {
                    return crud_service.get_me('categories');
                }
            }
        })
        .state('logout', {
            url: '/logout',
            controller: function (crud_service, $location, $rootScope) {
               var id = window.localStorage.getItem('userId');
                var promise = crud_service.delete_me('access/'+id);
                return promise.then(function (response) {
                        window.localStorage.removeItem('token');
                        window.localStorage.removeItem('userId');
                        $rootScope.logged = false;
                        $location.path('/login');
                    },
                    function (response) {
                        alert("error while logging out");
                    });
            }
        });

    $httpProvider.interceptors.push('intercepter');
    flashProvider.errorClassnames.push('alert-danger');
});

quizApp.run(function($rootScope, $location, $state) {
    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {

            if(window.localStorage.getItem('token') === null || window.localStorage.getItem('token') == undefined){
                $location.path('/login');
                $rootScope.logged = false;
            } else if
                (window.localStorage.getItem('token') != undefined) {
                $rootScope.logged = true;
                if (toState.name == 'login') {
                    if (window.localStorage.admin == "1") {
                        $location.path('/admin_home');
                    } else if (window.localStorage.admin == "0") {
                        $location.path('/user_home');
                    }
                }
                else if (stateChecked(toState.name, 'adminHome') && window.localStorage.admin == "0") {
                    //$location.path('/user_home/profile');
                    event.preventDefault();
                    $state.go('userHome.profile');
                } else if (stateChecked(toState.name, 'userHome') && window.localStorage.admin == "1") {
                    //$location.path('/admin_home/quizzes');
                    event.preventDefault();
                    $state.go('adminHome.quizzesList');
                }
            }
        });
});

function stateChecked(state,view){
    var splited = [];
     splited = state.split('.');
    if(splited[0] == view){
        return true;
    }
    return false;
}

quizApp.filter('secondsToMinutes', function () {
    return function(duration){
        var minutes = Math.floor(duration/60);
        var seconds = duration - minutes*60;
        var timeString = '';
        timeString+= minutes+ ' min : '+seconds +' sec';
        return timeString;
        //return '0'+minutes + ':'+ seconds;
    }
});