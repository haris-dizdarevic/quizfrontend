(function () {
  var adminHomeCtrl = function ($scope,crud_service, $modal, flash, $state, $timeout, $rootScope) {
      $state.go('adminHome.quizzesList');
      $rootScope.logged = true;
      $scope.createQuizModal = function () {
          var promise = crud_service.get_me('categories');
          promise.then(function (response) {
                  var modalInstance = $modal.open({
                    templateUrl: 'partials/createQuizModal.html',
                      controller: 'createQuizModalCtrl',
                      resolve: {
                          categories: function () {
                              return response.data;
                          }
                      }
                  });
                  modalInstance.result.then(function () {
                      
                  }, function () {
                      
                  })
          },
              function (error) {
                modalInstance.close();
              });

      };
      $scope.createCategoryModal = function(){
          var modalInstance = $modal.open({
              templateUrl: 'partials/createCategoryModal.html',
              controller: 'createCategoryModalCtrl'

          });
          modalInstance.result.then(function (response) {
            if(response.status == 409){
                flash.error = 'Kategorija s tim imenom već postoji';
            }
          }, function (error) {
              modalInstance.close();
          });
      };
      $scope.createUserModal = function () {
          var modalInstance = $modal.open({
              templateUrl: 'partials/createUserModal.html',
              controller: 'createUserModalCtrl'
          });
          modalInstance.result.then(function (response) {
                  if(response.status == 409){
                      flash.error = 'Korisnik s tim korisničkim imenom već postoji';
                  }else{
                      flash.success ='Korisnik '+ response.data.first_name +' '+ response.data.last_name + ' je uspjesno dodan.';
                      $timeout(function () {
                          $scope.$broadcast('userCreated');
                      });
                  }

          },
              function (error) {
                  modalInstance.close();
              });
      };
      $rootScope.$on('unauthorized', function () {
          $rootScope.logged = false;
          $state.go('login');
      });
  };
    quizApp.controller('adminHomeCtrl',adminHomeCtrl)
}());