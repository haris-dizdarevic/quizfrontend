(function () {
  var quizzesListCtrl = function ($scope, quizzes, $modal, flash, crud_service) {
      console.log(quizzes);
      $scope.quizzes = quizzes.data;
      $scope.addQuizToUserModal = function (quiz) {
          var modalInstance = $modal.open({
              templateUrl: 'partials/addQuizToUserModal.html',
              controller: 'addQuizToUserModalCtrl',
              resolve: {
                  quiz: function () {
                      return quiz;
                  }
              }
          });
          modalInstance.result.then(function (response) {
                  if(response.status == 404){
                      flash.error = 'Korisnik s tim korisničkim imenom ne postoji';
                  }else{
                      console.log(response);
                      flash.success = 'Uspješno dodan kviz korisniku '+ response.assigned_quiz.username +'.'
                  }
              },
              function (error) {

              });
      };
      $scope.removeQuiz = function (quiz) {
          var promise = crud_service.delete_me('quizzes/'+quiz.id);
          promise.then(function () {
                  loadQuizzes();
                  flash.success = 'Kviz je uspjesno obrisan';
              },
              function () {
                  alert("Something went wrong");
              });
      };
      function loadQuizzes(){
          var promise = crud_service.get_me('quizzes');
          promise.then(function (response) {
                  $scope.quizzes = response.data;
              },
              function (error) {
                  alert('Something went wrong');
              });
      }
  };
    quizApp.controller('quizzesListCtrl',quizzesListCtrl)
}());