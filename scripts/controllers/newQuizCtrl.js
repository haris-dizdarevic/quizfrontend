(function () {
    var newQuizCtrl = function ($scope, crud_service,$rootScope) {
        $scope.visible = true;
        $scope.questions = [];
        $scope.correct = false;
        $scope.questionBtn = true;
        $scope.saveQuestion = function () {
            var data= {'question':{
                'quiz_id':$rootScope.newQuizId,
                'question_text':$scope.newQuestion
            }};
            var promise = crud_service.post_me('questions',data);
            promise.then(function (response) {
                $scope.questions.push(response.data);
                    $scope.visible = false;
                    $scope.newQuestion = '';
                    $scope.questions[$scope.questions.length-1].answers = [];
            },
                function (error) {
                    alert('nije ok');
                });
        };

        $scope.saveAnswer = function () {
            var data = {'answer':{
                'question_id':$scope.questions[$scope.questions.length-1].id,
                'answer_text':$scope.newAnswer,
                'correct':false
            }};
            var promise = crud_service.post_me('answers',data);
            promise.then(function (response) {
                $scope.questions[$scope.questions.length-1].answers.push(response.data);
                    $scope.newAnswer = '';
                    $scope.correct = false;
                    var numberOfAnswers = $scope.questions[$scope.questions.length-1].answers.length;
                    if(numberOfAnswers >= 3){
                       checkAnswerTrue(numberOfAnswers);
                    }
            },
                function (error) {
                        alert('nije Ok');
                });

        };
        
        $scope.createNewQuestion = function () {
            $scope.visible = true;
            $scope.questionBtn = true;
        };

        $scope.answerUpdate = function (answer) {
            var data = {'answer': answer};
            var promise = crud_service.put_me('answers/'+answer.id,data);
            promise.then(function (response) {
                    if( $scope.questions[$scope.questions.length-1].answers.length >= 3){
                        checkAnswerTrue( $scope.questions[$scope.questions.length-1].answers.length);
                    }
            },
                function (error) {
                    alert('Something went wrong');
                })
        };

        $scope.removeAnswer = function (answer) {
            var promise = crud_service.delete_me('answers/'+answer.id);
            promise.then(function () {
                for (var i=0; i<$scope.questions.length; i++){
                    for(var j=0; j<$scope.questions[i].answers.length; j++){
                        if($scope.questions[i].answers[j].id == answer.id){
                            $scope.questions[i].answers.splice(j,1);
                        }
                    }
                }
            },
                function () {
                    alert('Something went wrong');
                });

        };

        function checkAnswerTrue(numberOfAnswers){
            var counter = 0;
            for(var i = 0; i< numberOfAnswers; i++){
                if($scope.questions[$scope.questions.length-1].answers[i].correct == true){
                    counter++;
                }
                if(counter > 0){
                    $scope.questionBtn = false;
                }
            }
        }
    };
    quizApp.controller('newQuizCtrl',newQuizCtrl)
}());