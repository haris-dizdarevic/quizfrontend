(function(){
    var userHomeCtrl = function($scope, $state, $modal, crud_service, $timeout, $rootScope){
        $state.go('userHome.profile');
        $rootScope.logged = true;
        $scope.changePasswordModal = function () {
            var promise = crud_service.get_me('users/'+window.localStorage.getItem('userId'));
            promise.then(function (response) {
                    var modalInstance = $modal.open({
                        templateUrl: 'partials/changePasswordModal.html',
                        controller: 'changePasswordModalCtrl',
                        resolve:{
                            user: function () {
                                return response.data;
                            }
                        }
                    });

                });
        }
        var timerCount = 300;
        var quizCountdown;
        $scope.countDown = function(){
            if (timerCount < 0){
                $scope.$broadcast('countDownFinished');
            }else{
                $scope.countDownLeft = timerCount;
                timerCount--;
                quizCountdown = $timeout($scope.countDown,1000);
            }
        };
        $scope.stopCounting = function () {
            $timeout.cancel(quizCountdown);
            timerCount = 300
        };

        $scope.countDownLeft = timerCount;
        $rootScope.$on('unauthorized', function () {
            $rootScope.logged = false;
            $state.go('login');
        });

    };


    quizApp.controller('userHomeCtrl',userHomeCtrl);
}());