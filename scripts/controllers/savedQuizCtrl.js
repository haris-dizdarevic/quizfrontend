(function () {
  var savedQuizCtrl = function ($scope, quiz, selectedAnswers, answers, crud_service, $state) {
        $scope.questions = quiz.data.quiz.questions;
        $scope.category = quiz.data.quiz.category.category_name;
        $scope.type = quiz.data.quiz.quiz_type;
        $scope.answers = answers.data;
        $scope.degreeOfAccuracy = [];

      //console.log('answers',$scope.answers[0].question_id);
      //console.log('quiz',quiz.data.quiz);
      //console.log($scope.questions[0].answers.length);
      if($scope.type == 'select'){
          showCheckedAnswers();
      }else if($scope.type == 'input'){
          addAnswerToQuestion();
          console.log('odgovori',$scope.questions);
      }

      $scope.grade = function () {
       var score = checkScore();
          var data = {'result': {'score': score}};
          var promise = crud_service.put_me('results/'+quiz.data.id, data);
          promise.then(function (response) {
            $state.go('adminHome.savedQuizzes');
          }, function (error) {
              alert('Something went wrong');
          });
      };
        function showCheckedAnswers() {
            for(var i=0; i<selectedAnswers.data.length; i++){
                for(var j=0; j<$scope.questions.length; j++){
                    for(var k=0; k<$scope.questions[j].answers.length; k++){
                        if($scope.questions[j].answers[k].id == selectedAnswers.data[i].answer_id){
                            $scope.questions[j].answers[k].isChecked = true;
                        }
                    }
                }
            }
        }

        function addAnswerToQuestion(){
            for (var i=0; i<$scope.questions.length; i++){
               for(var j=0; j<$scope.answers.length; j++){
                   if($scope.questions[i].id == $scope.answers[j].question_id){
                       $scope.questions[i].answers = $scope.answers[j];
                   }
               }
            }
        }

        function checkScore(){
            var maxScore = $scope.questions.length;
            var score = 0;
            var users_score= 0;
            for(var i = 0; i<$scope.questions.length; i++){
                score += Number($scope.degreeOfAccuracy[i]);
            }
            users_score = parseFloat(score)/parseFloat(maxScore);
            return users_score;
        }
  };
    quizApp.controller('savedQuizCtrl', savedQuizCtrl);
}());