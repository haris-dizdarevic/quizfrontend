(function () {
    var changePasswordModalCtrl = function ($scope, crud_service, user, flash) {

        $scope.savePass = function () {
            var data = {'user': $scope.user};
            var promise = crud_service.put_me('users/'+user.id,data);
            promise.then(function (response) {
                if(response.status == 401){
                    flash.error = 'Stara lozinka nije tačna';
                }else{
                    flash.success = 'Uspješno ste promijenuli lozinku';
                }
            },
                function () {

                });
        }

    };
    quizApp.controller('changePasswordModalCtrl',changePasswordModalCtrl)
}());