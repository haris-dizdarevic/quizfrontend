(function () {
  var updateUserModalCtrl = function ($scope, crud_service, user, $modalInstance) {
      $scope.user = user;
      if(user.role == 'admin'){
          $scope.isAdmin = true;
      }else{
          $scope.isAdmin = false;
      }

      $scope.updateUser = function () {
          if($scope.isAdmin){
              $scope.user.role = 'admin';
          }else{
              $scope.user.role = 'user';
          }
          var data = {'user': $scope.user};
          var promise = crud_service.put_me('users/'+user.id, data);
          promise.then(function (response) {
                  $modalInstance.close(response);
          },
              function (error) {
                  $modalInstance.close();
              });
      };
  };
    quizApp.controller('updateUserModalCtrl', updateUserModalCtrl)
}());