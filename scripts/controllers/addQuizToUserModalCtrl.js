(function () {
  var addQuizToUserModalCtrl = function ($scope, crud_service,quiz, $modalInstance, flash) {

      $scope.saveAssignedQuiz = function () {
          var data = {'assigned_quiz':{
              'user_id': quiz.user_id,
               'username':$scope.username,
                'quiz_id':quiz.id,
                'active': 1
          }};
          var promise = crud_service.post_me('assigned_quizzes',data);
          promise.then(function (response) {
              $modalInstance.close(data);
          },
              function (error) {
                  $modalInstance.close(error);
              });
      }

  };
    quizApp.controller('addQuizToUserModalCtrl', addQuizToUserModalCtrl);
}());