(function () {
  var createQuizModalCtrl = function ($scope,crud_service,categories, $state, $modalInstance,$rootScope) {
        $scope.categories = categories;
        var type = 'input';
        $scope.saveQuiz = function () {
            if ($scope.isChecked){
                type = 'select'
            }
            var data = {'quiz':{
                'user_id':window.localStorage.getItem('userId'),
                'category_id':$scope.selectedCategory,
                'active':false,
                'date':Date.now(),
                'quiz_type': type
            }};
            var promise = crud_service.post_me('quizzes',data);
            promise.then(function (response) {
                console.log(response);
                $rootScope.newQuizId = response.data.id;
                if(response.data.quiz_type == 'select'){
                    $state.go('adminHome.quizzes');
                }else if (response.data.quiz_type == 'input'){
                    $state.go('adminHome.inputQuiz');
                }
                    $modalInstance.close(response);


                },
                function (error) {
                    alert('Something went wrong');
                });
        };
  };
    quizApp.controller('createQuizModalCtrl',createQuizModalCtrl)
}());
