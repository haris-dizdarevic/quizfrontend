(function () {
  var usersListCtrl = function ($scope, crud_service, users, $modal, flash) {
      console.log(users.data);
      $scope.users = users.data;
      $scope.$on('userCreated', function () {
          getUsersList();
      });

      $scope.updateUserModal = function (user) {
          var modalInstance = $modal.open({
              templateUrl: 'partials/updateUserModal.html',
              controller: 'updateUserModalCtrl',
              resolve:{
                  user: function () {
                      var userCopy = angular.copy(user);
                      return userCopy;
                  }
              }
          });
          modalInstance.result.then(function (response) {
              getUsersList();
              flash.success = 'Korisnik ' + response.data.first_name + ' ' + response.data.last_name + ' je uspjesno uredjen';
          });
      };

      $scope.removeUser = function (user) {
        var promise = crud_service.delete_me('users/'+user.id);
          promise.then(function () {
                  getUsersList();
                  flash.success = 'Korisnik ' + user.first_name +' '+ user.last_name + ' je uspjesno obrisan';
          },
              function (error) {
                  alert('Something went wrong');
              });
      };

      getUsersList();
      function getUsersList() {
        var promise = crud_service.get_me('users');
          promise.then(function (response) {

              $scope.users = response.data
          },
              function (error) {
                  alert('Something went wrong');
              });
      }

  };
    quizApp.controller('usersListCtrl',usersListCtrl)
}());