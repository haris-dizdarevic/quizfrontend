(function () {
  var createUserModalCtrl = function ($scope,crud_service,flash, $modalInstance) {
      $scope.isAdmin = false;
        $scope.saveNewUser = function () {
            if ($scope.isAdmin){
                $scope.user.role = 'admin';
            }else{
                $scope.user.role = 'user';
            }
            var data = {'user': $scope.user};
          var promise = crud_service.post_me('users',data);
            promise.then(function (response) {
                $modalInstance.close(response);
            }, function (error) {
                $modalInstance.close(error);
            });
        };
  };
    quizApp.controller('createUserModalCtrl',createUserModalCtrl)
}());