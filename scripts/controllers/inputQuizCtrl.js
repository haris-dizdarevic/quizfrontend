(function () {
  var inputQuizCtrl = function ($scope, crud_service, $rootScope) {
      $scope.questions = [];

      $scope.saveQuestion = function () {
          var data= {'question':{
              'quiz_id':$rootScope.newQuizId,
              'question_text':$scope.newQuestion
          }};
          var promise = crud_service.post_me('questions',data);
          promise.then(function (response) {
                  $scope.questions.push(response.data);
                  $scope.newQuestion = '';
              },
              function (error) {
                  alert('nije ok');
              });
      };
  };
    quizApp.controller('inputQuizCtrl', inputQuizCtrl);
}());