(function () {
  var categoriesCtrl = function ($scope, crud_service, categories, $modal, flash) {
      $scope.categories = categories.data;

      $scope.updateCategory = function (category) {
          var modalInstance = $modal.open({
              templateUrl: 'partials/updateCategoryModal.html',
              controller: function ($scope, crud_service,category, $modalInstance) {
                $scope.category = category;
                   $scope.saveCategory = function () {
                      var data = {'category':$scope.category};
                    var promise = crud_service.put_me('categories/'+category.id,data);
                      promise.then(function (response) {
                          $modalInstance.close(response)
                      }, function (error) {
                          $modalInstance.close(error);
                      });
                  };
              },
              resolve: {
                  category: function () {
                      var resolvedCategory = angular.copy(category);
                      return resolvedCategory;
                  }
              }
          });
          modalInstance.result.then(function (response) {
              getCategoryList();
              flash.success = 'Kategorija ' + response.data.category_name + ' je uređena ' ;
          });

      };
      function getCategoryList(){
          var promise = crud_service.get_me('categories');
          promise.then(function (response) {
              $scope.categories = response.data;
          },
              function (error) {
                  alert('Something went wrong');
              });
      }

       $scope.deleteCategory = function (category) {
          var promise = crud_service.delete_me('categories/'+category.id);
          promise.then(function (response) {
              getCategoryList();
              flash.success = 'Kategorija ' +category.category_name+ ' je uspješno obrisana';
          }, function (error) {
              alert('Something went wrong');
          });
      }
  };

    quizApp.controller('categoriesCtrl',categoriesCtrl);
}());