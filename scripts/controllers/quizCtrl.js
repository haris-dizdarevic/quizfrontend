(function(){

    var quizCtrl = function($scope, quiz, crud_service, $modal){
        var score = 0;
        var highestScore = 0;
        var array = [];
        var nekiArray = [];
        $scope.stopCounting();
        $scope.countDown();
        $scope.finishQuiz = function(){
            checkHighestScore();
            checkAnswer();
            var data = {};
            data.user_id = window.localStorage.getItem('userId');
            data.quiz_id = $scope.id;
            data.score = (parseFloat(score)/parseFloat(highestScore)).toFixed(2)*100;
            var promise = crud_service.post_me('results',{'result': data});
            promise.then(function(response){
                console.log("Post result success",response);
                findChecked();
                saveQuiz(response.data.id);
                console.log('highestScore', highestScore);
                console.log('score', score);
                $scope.stopCounting();
                showScore();
            },function(error){
                console.log("Post result error",error);
            });
        };
        $scope.$on('countDownFinished', function () {
           $scope.finishQuiz();
        });
        $scope.limit = 2;
        $scope.checked = 0;

        console.log("quiz",quiz);

        $scope.id = quiz.data.id;

        $scope.category = quiz.data.category.category_name;

        $scope.questions = quiz.data.questions;



        function findChecked(){
            array = [];
            for (var i = 0; i < $scope.questions.length; i++ ) {
                for(var j=0; j<$scope.questions[i].answers.length; j++){
                    if($scope.questions[i].answers[j].isChecked == true) {
                        array.push($scope.questions[i].answers[j].id);
                    }
                }
            }
        }

        function checkHighestScore(){
            for (var i = 0; i < $scope.questions.length; i++ ) {
                for(var j=0; j<$scope.questions[i].answers.length; j++){
                    if($scope.questions[i].answers[j].correct == true){
                        highestScore++;
                    }
                }
            }
        }

        function saveQuiz(result_id){
            for(var z=0; z<array.length; z++) {
                var data = {"saved_quiz": {"result_id": result_id,"answer_id": array[z]}};
                var promise = crud_service.post_me('saved_quizzes',data);
                promise.then(function(response){
                    console.log("Post save quiz success",response);
                },function(error){
                    console.log("Post save quiz error",error);
                });
            }
        }

        function maxCorrectAnswerPerQuestion(question){
            var counter = 0;
            for(var i = 0; i< question.answers.length; i++){
                if(question.answers[i].correct == true){
                    counter++;
                }
            }
            return counter;
        }
        function showScore(){
            var modalInstance = $modal.open({
                templateUrl: 'partials/showResultModal.html',
                backdrop: 'static',
                controller: function ($scope, score, highestScore, $modalInstance,$state) {
                    $scope.result = (parseFloat(score)/highestScore).toFixed(4);
                    $scope.result*=100;
                    $scope.closeShowResultModal = function () {
                        $modalInstance.close();
                        $state.go('userHome.listOfQuizzes');
                    }

                },
                resolve: {
                    score: function(){
                        return score;
                    },
                    highestScore: function () {
                        return $scope.questions.length;
                    }
                }
            });
        }



        function checkAnswer(){
            for(var i = 0; i<$scope.questions.length; i++){
                var scoreQuestion = 0;
                for(var j = 0 ; j < $scope.questions[i].answers.length; j++){
                    if($scope.questions[i].answers[j].isChecked && $scope.questions[i].answers[j].correct){
                        scoreQuestion++;
                    }
                    else if($scope.questions[i].answers[j].isChecked == true && $scope.questions[i].answers[j].correct == false){
                        scoreQuestion--;
                    }
                }
                if(scoreQuestion == maxCorrectAnswerPerQuestion($scope.questions[i])){
                    score++;
                }

            }
        }
    };


    quizApp.controller('quizCtrl',quizCtrl);
}());