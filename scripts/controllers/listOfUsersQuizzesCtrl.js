(function () {
  var listOfUsersQuizzesCtrl = function ($scope, quizzes, $location, crud_service) {
      console.log('quizzes',quizzes.data);
      $scope.quizzes = quizzes.data;

      $scope.showQuiz = function(quiz){
          if(quiz.quiz.quiz_type == 'select'){
              $location.path('user_home/quiz/'+quiz.quiz.id);
          }else if(quiz.quiz.quiz_type == 'input'){
              $location.path('user_home/input/quiz/'+quiz.quiz.id)
          }
          var data = {'assigned_quiz':{
             'active': false
          }};
          console.log('data',data);
           crud_service.put_me('assigned_quizzes/'+quiz.id, data);

      };
  };
    quizApp.controller('listOfUsersQuizzesCtrl', listOfUsersQuizzesCtrl)
}());