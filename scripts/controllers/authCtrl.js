(function(){

    var authCtrl = function($scope, crud_service,$location, flash, $state, $rootScope){
        if(window.localStorage.admin == "1"){
            $state.go('adminHome');
        }else if(window.localStorage.admin == "0"){
            $state.go('userHome');
        }
        console.log('logged',$rootScope.logged);
        $scope.logUser = function(){
            var data = {"access":$scope.user};
            var promise = crud_service.post_me('access',data);
            promise.then(function(response){
                window.localStorage.setItem('token',response.data.auth_token);
                window.localStorage.setItem('userId',response.data.id);
                if(response.data.role == 'admin'){
                    window.localStorage.setItem('admin',1);
                    $location.path('/admin_home');
                }else{
                    window.localStorage.setItem('admin',0);
                    $location.path('/user_home');
                }
                $rootScope.logged = true;
            },function(error){
                flash.error = 'Korisnički podaci nisu validni.';
            });
        };

    };

    quizApp.controller('authCtrl',authCtrl);
}());