(function () {
  var createCategoryModalCtrl = function ($scope,crud_service,$modalInstance) {
        $scope.saveCategory = function () {
            var data ={'category':$scope.category};
            var promise = crud_service.post_me('categories',data);
            promise.then(function (response) {
                    $modalInstance.close(response);
            },
                function (error) {
                    $modalInstance.close(error);
                });
        };
  };
    quizApp.controller('createCategoryModalCtrl',createCategoryModalCtrl)
}());