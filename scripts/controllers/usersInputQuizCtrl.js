(function () {
    var usersInputQuizCtrl = function ($scope, crud_service, quiz,$state) {
        $scope.quiz = quiz.data;
        $scope.questions = quiz.data.questions;
        $scope.questions.answer =[];
        $scope.stopCounting();
        $scope.countDown();
        $scope.finishQuiz = function () {
            var data = {'result':{
                'user_id':window.localStorage.getItem('userId'),
                'quiz_id':$scope.quiz.id
            }};
            var promise = crud_service.post_me('results',data);
            promise.then(function (response) {
                for(var i = 0; i<$scope.questions.length; i++){
                    var data = {'saved_answer':{
                        'result_id': response.data.id,
                        'users_answer': $scope.questions[i].answer,
                        'question_id': $scope.questions[i].id
                    }};
                    var promise = crud_service.post_me('saved_answers',data);
                    promise.then(function (response) {
                        $scope.stopCounting();
                        $state.go('userHome.listOfQuizzes');
                    });
                }
            },
                function (error) {
                    alert("something went wrong");
                });
        };
        $scope.$on('countDownFinished', function () {
            $scope.finishQuiz();
        })
    };
    quizApp.controller('usersInputQuizCtrl', usersInputQuizCtrl);
}());